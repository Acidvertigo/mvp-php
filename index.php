<?php

//set error reporting
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

define(APP_ROOT, __DIR__ . '/src/App');
define(TEMPLATE_DIR, APP_ROOT . '/Templates/');
define(APP_NAMESPACE, 'Core\\App\\');

require_once 'vendor/autoload.php';
require_once 'routes.php';

use Core\Components\Factories\ServiceFactory;
use Core\Components\Renderers\ViewRenderer;
use Core\Components\Views\View;
use Core\Services\SystemMessage;
use WoohooLabs\Harmony\Exception\MethodNotAllowed;
use WoohooLabs\Harmony\Exception\RouteNotFound;
use WoohooLabs\Harmony\Harmony;
use WoohooLabs\Harmony\Middleware\DiactorosResponderMiddleware;
use WoohooLabs\Harmony\Middleware\DispatcherMiddleware;
use WoohooLabs\Harmony\Middleware\FastRouteMiddleware;
use Zend\Diactoros\Response;
use Zend\Diactoros\Response\SapiEmitter;
use Zend\Diactoros\ServerRequestFactory;

$harmony = new Harmony(ServerRequestFactory::fromGlobals(), new Response());

$harmony
    ->addMiddleware(new DiactorosResponderMiddleware(new SapiEmitter()))
    ->addMiddleware(new FastRouteMiddleware($router))
    ->addMiddleware(new DispatcherMiddleware());

try
{
    $harmony();
} catch (RouteNotFound $e)
{
    $view = new View(new ViewRenderer);
    echo $view->output('not_found');
} catch (MethodNotAllowed $e)
{
    $view = new View(new ViewRenderer);
    echo $view->output('method_notallowed');
} catch (\Exception $e)
{
    $message = new SystemMessage;
    echo $message($e);
}