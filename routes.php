<?php

use Core\App\Views;
use Core\Components\Builders\AppBuilder;
use Core\Components\Builders\AppDirector;
use Core\Components\Renderers\ViewRenderer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

$router = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r)
{

    $app = function($context)
    {
        $dir  = new AppDirector();
        $view = APP_NAMESPACE . 'Views\\' . ucfirst($context);
        return $dir->build(new AppBuilder(new $view(new ViewRenderer)));
    };

    $r->addRoute('GET', '/me', function(ServerRequestInterface $request,
                                            ResponseInterface $response)
                                            use ($app)
    {
        $response->getBody()->write('Welcome to the real world!');

        return $response;
    });

    $r->addRoute('GET', '/users', function(ServerRequestInterface $request,
                                            ResponseInterface $response)
                                            use ($app)
    {
        $page = $app('Main');
        return $page($request, $response);
    });
});