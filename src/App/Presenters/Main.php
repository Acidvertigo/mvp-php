<?php

namespace Core\App\Presenters;

use Core\Components\Presenters\Presenter;
use Core\Components\Views\IView;
use Core\Components\Domain\Models\Model;

class Main extends Presenter
{
    public function __construct(IView $view, Model $model)
    {
        parent::__construct($view, $model);
    }
}