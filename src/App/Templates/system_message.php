<!doctype html>
<html lang = "en">
    <head>
        <style type="text/css">
            .exception
            {
                            position: relative;
                            font-family: monospace;
                            text-decoration: none;
                            background-color: black;
                            color: green;
                            font-size: 1.5em;
            }
        </style>
    </head>
    <body>
        <div class ="exception">
             <p><u>Application Error</u></p>
             <p><b>Exception Message: </b><?php echo $this->escape('message'); ?></p>
             <p><b>File: </b><?php echo $this->escape('file'); ?></p>
             <p><b>Line: </b><?php echo $this->escape('line'); ?></p>
             <p><b>Error Code:</b> <?php echo $this->escape('errorCode'); ?></p>
             <p><b>Stack Trace: </b><?php echo $this->escape('stack'); ?></p>
        </div>
    </body>
</html>