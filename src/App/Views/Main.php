<?php

namespace Core\App\Views;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Core\Components\Renderers\IViewRenderer;
use Core\Components\Views\View;

class Main extends View
{
    public function __construct(IViewRenderer $renderer)
    {
        parent::__construct($renderer);
    }

    public function __invoke(ServerRequestInterface $request,
                                ResponseInterface $response)
    {
        $this->presenter->execute($request, $response);
        $response->getBody()->write($this->output('index'));

        return $response;
    }
}