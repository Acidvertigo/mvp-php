<?php
/***
 * Main application configuration file
 */

return [
    'db' => [
        'dsn'   => 'mysql:host=' . getenv('IP') . ';dbname=' . getenv('DBNAME'),
        'user'  => getenv('USER'),
        'passw' => getenv('PASSW'),
        ],
    'languages' => [
        'en' => 'english',
        'it' => 'italian'
        ],
    'site' => [
        'domain' => 'https://' . getenv('DOMAIN'),
        ],
    'session' => [
        'name'            => 'mysession',
        'cookie.domain'   => '.' . getenv('DOMAIN'),
        'cookie.lifetime' => 3600,
        'cookie.path'     => '/',
        'secure'          => false,
        'httponly'        => false
        ]
];