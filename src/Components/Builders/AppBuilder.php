<?php
/***
 * @package Builders
 **/

namespace Core\Components\Builders;

use Core\Components\Domain\Models\Model;
use Core\Components\Views\IView;

class AppBuilder implements IAppBuilder
{
    /** @var IView $view **/
    private $view;

    /***
     * @param IView $view
     */
    public function __construct(IView $view)
    {
        $this->view = $view;
    }

    /***
     * @return \Core\App\Presenters\IPresenter
     */
    public function makePresenter(Model $model)
    {
        $class = APP_NAMESPACE . 'Presenters\\' . substr(strrchr(get_class($this->view), '\\'), 1);
        return new $class($this->view, $model);
    }

    /***
     * @param \PDO $dbAdapter
     * @return \Core\App\Domain\Models\Model
     */
    public function makeModel($dbAdapter)
    {
        $class = APP_NAMESPACE . 'Domain\\Models\\' . substr(strrchr(get_class($this->view), '\\'), 1);
        return new $class($dbAdapter);
    }

    /**
     * @return IView
     */
    public function getResults()
    {
        return $this->view;
    }
}