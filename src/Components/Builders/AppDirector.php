<?php

namespace Core\Components\Builders;

use Core\Componente\Views\IView;
use Core\Components\Builders\IAppBuilder;
use Core\Components\Builders\ServiceBuilder;
use Core\Services\SystemMessage;

class AppDirector
{
    /** @var array $services **/
    private $services = [];

    /**
     * @param IAppBuilder
     * @return \Core\Components\Views\IView
     */
    public function build(IAppBuilder $builder)
    {
        try
        {
            $this->buildServices(new ServiceBuilder);
        } catch (\Exception $e)
        {
            $message = new SystemMessage;
            echo $message($e);
        }

        $model = $builder->makeModel($this->services['db']);
        $builder->makePresenter($model);

        return $builder->getResults();
    }

    /**
     * @param IServiceBuilder
     */
    private function buildServices(IServiceBuilder $builder)
    {
        $builder->build('Config');
        $this->services['config'] = $builder->getResults();

        $builder->build('Database', [$this->services['config']->get('db')]);
        $this->services['db'] = $builder->getResults();

        $builder->build('Language', [$this->services['config']->get('languages')]);
        $this->services['language'] = $builder->getResults();

        $builder->build('Session', [$this->services['config']->get('session')]);
        $this->services['session'] = $builder->getResults();

        $this->services['db']->getDb();
    }
}