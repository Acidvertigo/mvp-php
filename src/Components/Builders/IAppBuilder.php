<?php
/***
 * @package Builders
 **/
namespace Core\Components\Builders;

use Core\Components\Domain\Models\Model;
use Core\Components\Views\IView;

interface IAppBuilder
{
    /**
     * @return \Core\Components\Presenters\IPresenter
     */
    public function makePresenter(Model $model);

    /**
     * @return Model
     */
    public function makeModel($dbAdapter);

    /**
     * @return IView
     */
    public function getResults();
}