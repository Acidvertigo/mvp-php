<?php
/***
 * @package Builders
 **/
namespace Core\Components\Builders;

interface IServiceBuilder
{
    /**
     * @param string $className
     *
     * @return void
     */
    public function build($className, array $args = []);
    /**
     * @return object
     */
    public function getResults();
}