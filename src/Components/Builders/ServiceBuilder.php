<?php
/***
 * @package Builders
 **/
namespace Core\Components\Builders;

use Core\Components\Exceptions\ServiceNotFoundException;

class ServiceBuilder implements IServiceBuilder
{
    /** @var $service **/
    private $service;

    /**
     * @param string $className
     * @param null|array $args
     * @return void
     */

    public function build($className, array $args = [])
    {
        $service = 'Core\\Services\\' . ucfirst($className);

        if (!class_exists($service))
        {
            throw new ServiceNotFoundException($service);
        }

        if (!empty($args))
        {
            $ref = new \ReflectionClass($service);
            $obj = $ref->newInstanceArgs($args);
        } else
        {
            $obj = new $service;
        }

        $this->service = $obj;
    }

    /**
     * @return object
     */
    public function getResults()
    {
        return $this->service;
    }
}