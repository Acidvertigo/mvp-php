<?php
/***
 * @package Model
 **/
namespace Core\Components\Domain\Models;

use Core\Components\Presenters\IPresenter;
use Core\Services\Database;

class Model
{
    /** @var int $id **/
    protected $id;
    /** @var IPresenter $presenter **/
    protected $presenter;
    /** @var array $models **/
    protected $models = [];
    /** @var Database $dbAdapter **/
    protected $dbAdapter;

    /**
     * @param Database $dbAdapter
     **/
    public function __construct(Database $dbAdapter)
    {
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * @return int
     **/
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return int
     * @throws Exception
     */
    public function setId($id)
    {
        if (!is_null($this->id))
        {
            throw new \Exception('ID is immutable');
        }
        return $this->id = $id;
    }

    /**
     * @param IPresenter $presenter
     */
    public function setPresenter(IPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /**
     * @param Model $model
     */
    public function addModel(Model $model)
    {
        $this->models[get_class($model)] = $model;
    }

    /**
     * @param string $model
     * return Model
     **/    
    public function getModel($model)
    {
        return $this->models[$model];
    }
}