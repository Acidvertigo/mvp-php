<?php
/***
 * @package Exceptions
 **/
namespace Core\Components\Exceptions;

class FileNotFoundException extends \Exception
{
    public function __construct($filename = '')
    {
        parent::__construct('Unable to find file ' . $filename);
    }
}