<?php
/***
 * @package Exceptions
 **/
namespace Core\Components\Exceptions;

/***
 * Thrown when a View renderer object cannot load its associated template file
 * @package Core
 * @subpackage Exceptions
 */
class RendererTemplateFileNotFoundException extends \Exception
{
    public function __construct($filename)
    {
        parent::__construct('Cannot find template file ' . $filename);
    }
}