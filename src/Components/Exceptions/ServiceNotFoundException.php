<?php
/***
 * @package Exceptions
 **/
namespace Core\Components\Exceptions;

class ServiceNotFoundException extends \Exception
{
    public function __construct($service)
    {
        parent::__construct('Service: ' . $service . ' not found.');
    }
}