<?php

namespace Core\Components\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

interface IMiddleware
{
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param callable $next
     * @return ResponseInterface
     **/
    public function __invoke(ServerRequestInterface $request,
                                ResponseInterface $response,
                                callable $next);
}