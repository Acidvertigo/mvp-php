<?php

namespace Core\Components\Presenters;

interface IPresenter
{

    /**
     * @return void
     */
    public function attachSubPresenter(IPresenter $presenter);
}