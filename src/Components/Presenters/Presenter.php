<?php
/***
 * Main presenter class. Acts as intermediary between the view and the model
 * @package Core
 * @subpackage Presenter
 */

namespace Core\Components\Presenters;

use Core\Components\Domain\Models\Model;
use Core\Components\Views\IView;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Presenter implements IPresenter
{
    private $subpresenter = [];
    private $view;
    private $model;

    public function __construct(IView $view, Model $model)
    {
        $this->view  = $view;
        $this->model = $model;
        $this->view->setPresenter($this);
        $this->model->setPresenter($this);
    }

    public function setup(array $data)
    {
        foreach ($data as $key => $value)
        {
            $this->view->set($key, $value);
        }
    }

    public function execute(ServerRequestInterface $request,
                            ResponseInterface $response)
    {
        $properties = $this->view->getAll();
    }

    /***
     * Attach a new presenter to array
     * @param IPresenter $presenter
     */
    public function attachSubPresenter(IPresenter $presenter)
    {
        $this->subpresenter[get_class($presenter)] = $presenter;
    }
}