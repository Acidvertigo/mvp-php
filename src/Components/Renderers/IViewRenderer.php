<?php

namespace Core\Components\Renderers;

use Core\Components\Views\IView;

interface IViewRenderer
{

    /**
     * @param IView $view
     * @return void
     */
    public function attachView(IView $view);

    /**
     * @param $file
     * @return string
     */
    public function render($file);

    /**
     * @param $templateFile
     * @return void
     */
    public function setTemplateFile($templateFile);
}