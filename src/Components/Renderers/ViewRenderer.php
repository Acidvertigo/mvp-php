<?php
/***
 * Renderer class
 * @package ViewRenderer
 */
namespace Core\Components\Renderers;

use Core\Components\Exceptions\RendererTemplateFileNotFoundException;
use Core\Components\Renderers\IViewRenderer;
use Core\Components\Views\IView;

class ViewRenderer implements IViewRenderer
{

    const FILE_EXT = '.php';
    /** @var array $views **/
    protected $views = [];
    /** @var array $data **/
    protected $data = [];
    /** @var string $templateFile **/
    protected $templateFile = '';

    /***
     * View object container
     * @param IView $view
     */
    public function attachView(IView $view)
    {
        $this->views[get_class($view)] = $view;
    }

    /**
     * Set the template file to render, if needed. Checks if the file exists
     * and if the name of templatefile is not empty
     * @param string $templateFile
     * @throw \InvalidArgumentException
     */
    public function setTemplateFile($templateFile)
    {
        if (empty($templateFile))
        {
            new \InvalidArgumentException('Template filename cannot be empty');
        }

        if (!file_exists(TEMPLATE_DIR . $templateFile . self::FILE_EXT))
        {
            throw new RendererTemplateFileNotFoundException(TEMPLATE_DIR . $templateFile . self::FILE_EXT);
        }

        $this->templateFile = $templateFile;
    }

    /***
     * Flattens view data array
     */
    private function arrFlat()
    {
        foreach ($this->views as $view)
        {
            $this->data = array_merge($view->getAll(), $this->data);
        }
    }

    /***
     * Renders the template file
     * @param string $template
     * @throw \RendererTemplateFileNotFoundException
     * @return string
     */
    public function render($template)
    {
        $this->setTemplateFile($template);
        $this->arrFlat();

        ob_start();
        include TEMPLATE_DIR . $this->templateFile . self::FILE_EXT;
        $output = ob_get_clean();

        return $output;
    }

    /***
     * @param string $string
     * @return string
     */
    public function escape($string)
    {
        return htmlspecialchars($this->data[$string]);
    }

}