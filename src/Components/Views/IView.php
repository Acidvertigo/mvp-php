<?php
/***
 * View interface definition
 */
namespace Core\Components\Views;

interface IView
{
    /**
     * @return void
     */
    public function set($property, $value);

    /**
     * @return void
     */
    public function get($property);

    /**
     * @return void
     */
    public function getAll();
}