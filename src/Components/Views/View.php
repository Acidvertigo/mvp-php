<?php
/***
 * @package View
 */
namespace Core\Components\Views;

use Core\Components\Renderers\IViewRenderer;
use Core\Components\Views\IView;
use Core\Components\Presenters\IPresenter;
use Core\Components\Exceptions\RenderTemplateFileNotFoundException;
use Core\Services\SystemMessage;

class View implements IView
{
    /** @var array $properties **/
    protected $properties = [];
    /** @var IViewRenderer $renderer **/
    protected $renderer;
    /** @var IPresenter $presenter **/
    protected $presenter;

    /***
     * @param IViewRenderer $renderer
     */
    public function __construct(IViewRenderer $renderer)
    {
        $this->renderer = $renderer;
        $this->renderer->attachView($this);
    }

    /***
     * @param IPresenter $presenter
     */
    public function setPresenter(IPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /***
     * @param string $property
     * @param int|bool|string $value
     */
    public function set($property, $value)
    {
        $this->properties[$property] = $value;
    }

    /***
     * @param string $property
     * @return int|bool|string
     */
    public function get($property)
    {
        return $this->properties[$property];
    }

    /***
     * @return array
     */
    public function getAll()
    {
        return $this->properties;
    }

    /***
     * @return IViewRenderer
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /***
     * @param string $templateName
     * @return string $output
     */
    public function output($templateName)
    {
        try
        {
            $output = $this->renderer->render($templateName);
        } catch (\Exception $e)
        {
            $message = new SystemMessage;
            $output  = $message($e);
        }

        return $output;
    }
}