<?php
/***
 * @package Config
 */
namespace Core\Services;

use Core\Components\Exceptions\FileNotFoundException;

class Config
{
    /** @var array $config **/
    private $config = [];

    public function __construct()
    {
        if (!file_exists(APP_ROOT . '/config.php'))
        {
            throw new FileNotFoundException(APP_ROOT . '/config.php');
        }

        $this->config = include APP_ROOT . '/config.php';
    }

    /***
     * @parameter string $key
     * @return int|bool|string
     */
    public function get($key)
    {
        if (empty($key) || !isset($this->config))
        {
            throw new \InvalidArgumentException('Config Key empty or not found');
        }

        return $this->config[$key];
    }

}