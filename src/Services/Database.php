<?php
/**
 * @package Database
 */
namespace Core\Services;

class Database
{
    /** @var array $config **/
    private $config;
    /** @var \PDO $connection **/
    private static $connection;

    /**
     * @parameter array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function connect()
    {
        try
        {
            self::$connection = new \PDO($this->config['dsn'],
                                        $this->config['user'],
                                        $this->config['passw'],
                                        [
                                            \PDO::MYSQL_ATTR_INIT_COMMAND
                                            => 'SET NAMES utf8',
                                        ]);
        } catch (\PDOException $e)
        {
            echo $e;
        }
    }

    /**
     * @return \PDO
     */
    public function getDb()
    {
        if (empty(self::$connection))
        {
            $this->connect();
        }

        return self::$connection;
    }

}