<?php
/***
 * @package Language
 */
namespace Core\Services;

use Core\Components\Exceptions\FileNotFoundException;
use Core\Services\Config;

class Language
{
    private $langs = [];

    public function __construct(array $langs)
    {
        $this->langs = $langs;
    }

    /**
     * @return string
     */
    public function getFavLang()
    {
        $browserLanguage = substr(explode(',', getenv('HTTP_ACCEPT_LANGUAGE'))[0], 0, 2);
        return array_search($this->langs[$browserLanguage], $this->langs) ?: array_values(array_flip($this->langs))[0];
    }
}