<?php

namespace Core\Services;

class Session
{
    /** @var array $config **/
    private $config = [];

    /**
     *  @parameter array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;

        if (!isset($_COOKIE[$this->config['name']]))
        {
            $this->cookieSet();
        }
    }

    private function cookieSet()
    {
        session_set_cookie_params(
            $this->config['cookie.lifetime'],
            $this->config['cookie.path'],
            $this->config['cookie.domain'],
            $this->config['secure'],
            $this->config['httponly']
        );

        session_name($this->config['name']);
        session_start();
        $_COOKIE[$this->config['name']] = 'session cookie';
    }
}