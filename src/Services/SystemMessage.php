<?php
/***
 * Outputs formatted info from Exception thrown by the framework
 * @package Core
 * @subpackage Services
 */
namespace Core\Services;

use Core\Components\Renderers\ViewRenderer;
use Core\Components\Views\View;

class SystemMessage
{

    /***
     * @parameter \Exception $exception
     * @return string
     */
    public function __invoke(\Exception $exception)
    {
        $view = new View(new ViewRenderer);
        $view->set('message', $exception->getMessage());
        $view->set('errorCode', $exception->getCode());
        $view->set('file', $exception->getFile());
        $view->set('line', $exception->getLine());
        $view->set('stack', $exception->getTraceAsString());

        $output = $view->output('system_message');

        return $output;
    }

}